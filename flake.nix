{
  description = "Devshell for a CS1PC20 suitable c environment";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... }: 
    flake-utils.lib.eachDefaultSystem (
      system:
      let 
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [
            # Shell utils
            busybox
            usbutils
            pciutils
            git
            gnupg
            # Languages, compilers and build tooling
            gnumake
            gcc.cc.libgcc
            sccache
            # Debugging tooling
            gdb
            valgrind
            # tree sitters
            tree-sitter-grammars.tree-sitter-c
            tree-sitter-grammars.tree-sitter-cmake
            tree-sitter-grammars.tree-sitter-cpp
            # LSPs
            clang-tools
            # Editors
            vim
            doxygen
            texlive.combined.scheme-minimal
            plantuml
          ];
        };
      }
    );
}
