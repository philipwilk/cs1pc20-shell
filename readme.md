# CS1PC20-shell

Nix flake that should get you up and running with a development environment for the CS1PC20 module with no* configuration. This has the languages, compilers and editors necessary, without affecting your host system's configuration in any way.
As this is a Nix flake, it still requires that you have Nix installed and set up with the 'nix-command' and 'flakes' "experimental features" enabled. See below.
## MacOS or Linux users:
This can be done very easily on almost any system by running the [DeterminateSystems/nix-installer](https://github.com/DeterminateSystems/nix-installer), which has the nix command and flakes enabled OOTB.
Achieving this is as simple as running ``curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix | sh -s -- install`` in your respective command line.
## Windows users
You can also use nix via WSL2, but [it is recommended you enabled systemd in WSL first before running the command above](https://github.com/DeterminateSystems/nix-installer#in-wsl2).

## Entering the development shell.
Once Nix is installed and you have downloaded and entered this repo in your command line, you can enter a devlopment shell by running ``nix develop`` from inside the folder; Nix will download all the dependencies defined in this flake and create a virtual environment with everything you need to start coding in c.
Depending on your internet speed, this may take a moment, as it needs to download *everything* at once.
If you open a new terminal window or use the ``exit`` command, you will be put back into your regular shell, without any of the packages from the development environment. Simply re-run ``nix develop`` and it will put you back into the environment.